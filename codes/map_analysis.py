#All the below functions requires city tag value and these are several pipelines of mongodb quires

def pipeline_output(collection,city_names,db,method):
    for cty in city_names:
        result1 =db[collection].aggregate(method(cty))
        for each in result1:    
            print(each)
    print("\n")


####################################################################################################################
# ### To get the top contributors according to each city differently we use the pipeline query structure of mongo db
# #### Where to select different cities we use pipeline fuction given below for the rest of the queries
# #### we just need to update the p variable in pipeline function


def top_contributors(city):
    print "To get the top contributors:" ,city

    p= [{"$match":{"created.user":{"$exists":1},
                                          "city":city}},
                 {"$group": {"_id": {"City":"$city",
                                     "User":"$created.user"},
                            "contribution": {"$sum": 1}}},                            
                 {"$project": {'_id':0,
                               "City":"$_id.City",
                               "User_Name":"$_id.User",
                               "Total_contribution":"$contribution"}},
                 {"$sort": {"Total_contribution": -1}},
                 {"$limit" : 5 }]
    return p




####################################################################################################################

# ### To get the top Amenities in Hoodi and Bunkyo


def top_Amenities(city):
    print "To get the top Amenities in", city

    p = [{"$match":{"Additional Information.amenity":{"$exists":1},
                                          "city":city}},
                 {"$group": {"_id": {"City":"$city",
                                    "Amenity":"$Additional Information.amenity"},
                            "count": {"$sum": 1}}},
                 {"$project": {'_id':0,
                               "City":"$_id.City",
                               "Amenity":"$_id.Amenity",
                               "Count":"$count"}},
                 {"$sort": {"Count": -1}},
                 {"$limit" : 10 }]
    return p

####################################################################################################################


# Total nodes in database.
def Total_nodes(collection,city_names,db):
    print("2. Data Overview:")
    print("\n")
    print("Total nodes in database")
    for city in city_names:    
        print city,db[collection].find({'city':city}).count()
    print("\n")




# Number of node nodes.
def city_nodes(collection,city_names,db):
    print("Number of node nodes:")
    for city in city_names:    
        print city,db[collection].find({"type":"node",
                                    'city':city}).count()
    print("\n")


# Number of way nodes.
def total_ways(collection,city_names,db):
    print("Number of node nodes:")
    for city in city_names:    
        print city,db[collection].find({"type":'way',
                                    'city':city}).count()
    print("\n")


# Number of constributors.
def total_contributors(collection,city_names,db):
    print("Number of contributors")
    print len(db[collection].distinct("created.user"))
    print("\n")

####################################################################################################################

##  popular places of worship
def places_worship(city):
    print "Popular places of worship:" , city

    p = [{"$match":{"Additional Information.amenity":{"$exists":1},
                                  "Additional Information.amenity":"place_of_worship",
                                  "city":city}},
                       {"$group":{"_id": {"City":"$city",
                                          "Religion":"$Additional Information.religion"},
                                  "count":{"$sum":1}}},
                       {"$project":{"_id":0,
                                    "City":"$_id.City",
                                    "Religion":"$_id.Religion",
                                    "Count":"$count"}},
                       {"$sort":{"Count":-1}},
                       {"$limit":6}]
    return p


####################################################################################################################
## popular restaurants

def popular_restaurants(city):
    print "popular restaurants" ,city

    p = [{"$match":{"Additional Information.amenity":{"$exists":1},
                                 "Additional Information.amenity":"restaurant",
                                 "city":city}},
                      {"$group":{"_id":{"City":"$city",
                                        "Food":"$Additional Information.cuisine"},
                                 "count":{"$sum":1}}},
                      {"$project":{"_id":0,
                                  "City":"$_id.City",
                                  "Food":"$_id.Food",
                                  "Count":"$count"}},
                      {"$sort":{"Count":-1}}, 
                      {"$limit":6}]
    return p

####################################################################################################################


## popular fast food joints


def popular_fastfood(city):
    print "popular fast food joints" ,city

    p = [{"$match":{"Additional Information.amenity":{"$exists":1},
                                 "Additional Information.amenity":"fast_food",
                                 "city":city}},
                      {"$group":{"_id":{"City":"$city",
                                        "Food":"$Additional Information.cuisine"},
                                 "count":{"$sum":1}}},
                      {"$project":{"_id":0,
                                  "City":"$_id.City",
                                  "Food":"$_id.Food",
                                  "Count":"$count"}},
                      {"$sort":{"Count":-1}}, 
                      {"$limit":6}]
    return p

####################################################################################################################


##  ATM's near locality

def ATMS(city):
    print "ATM's near locality",city

    p = [{"$match":{"Additional Information.amenity":{"$exists":1},
                                 "Additional Information.amenity":"atm",
                                 "city":city}},
                      {"$group":{"_id":{"City":"$city",
                                        "Name":"$Additional Information.name:en"},
                                 "count":{"$sum":1}}},
                      {"$project":{"_id":0,
                                  "City":"$_id.City",
                                  "Name":"$_id.Name",
                                  "Count":"$count"}},
                      {"$sort":{"Count":-1}}, 
                      {"$limit":6}]
    return p


####################################################################################################################
import re
##  Martial arts or Dojo Center near locality
def martial_arts(city):
    print "Martial arts or Dojo Center near locality" ,city

    #Here i use regex to find the word dojo in Additional Information.name and Additional Information.amenity
    pat = re.compile(r'dojo', re.I)
    p=[{"$match":{ "$or": [ { "Additional Information.name": {'$regex': pat}}
                                                       ,{"Additional Information.amenity": {'$regex': pat}}]}}
                                ,{"$group":{"_id":{"City":"$city"
                                 , "Sport":"$Additional Information.name"}}}]
    return p

####################################################################################################################


## most popular shops.


def popular_shops(city):
    print "most popular shops",city
    p = [{"$match":{"Additional Information.shop":{"$exists":1},
                                          "city":city}},
                       {"$group":{"_id":{"City":"$city",
                                  "Shop":"$Additional Information.shop"},
                           "count":{"$sum":1}}},
                       {"$project": {'_id':0,
                                     "City":"$_id.City",
                                     "Shop":"$_id.Shop",
                                     "Count":"$count"}},
                       {"$sort":{"Count":-1}},
                       {"$limit":10}]
    return p


####################################################################################################################


## most popular supermarkets


def popular_supermarkets(city):
    print "most popular supermarkets" ,city

    p = [{"$match":{"Additional Information.shop":{"$exists":1},
                           "city":city,
                           "Additional Information.shop":"supermarket"}},
                       {"$group":{"_id":{"City":"$city",
                                  "Supermarket":"$Additional Information.name"},
                           "count":{"$sum":1}}},
                       {"$project": {'_id':0,
                                     "City":"$_id.City",
                                     "Supermarket":"$_id.Supermarket",
                                     "Count":"$count"}},
                       {"$sort":{"Count":-1}},
                       {"$limit":5}]
    return p




####################################################### X #############################################################





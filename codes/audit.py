import xml.etree.cElementTree as ET
import re
import pprint
from collections import defaultdict

print("Auditing Over-Abbreviated Names")
#Auditing Over-Abbreviated Names:
# Since the most of data being manually uploaded, there are lot of abbreviations in street names,locality names.
# Where they are filtered and replaced with full names.
# Add all the files in File list to test fix names
File=["tokyo1.osm","bangalore.osm"]
street_type_re = re.compile(r'\b\S+\.?$', re.IGNORECASE)


expected = ["Street", "Avenue", "Boulevard", "Drive", "Court","Apartment",
            "Place", "Square", "Lane", "Road", "Trail", "Parkway", "Commons",
           "Highway", "Loop", "Circle", "Walk", "Way", "Southwest", "Northeast",
           "Southeast", "Northwest"]

# UPDATE THIS VARIABLE
mapping = { "pl": "Place",
            "st": "Saint",
           "st ":"Saint",
            "ph" :"Phase",
            "apt" :"Apartment",
            "ku" :"City",
            't':"Town",
           "jr":"Junior",
           "d":"Door",
           "bld":"Building",
           "univ.": "University",
            "univ": "University",
           "bldg.":"Building",
           "bldg":"Building",
           "Co.":"Company",
           "co":"Company",
           "Co.,Ltd.":"Company Limited",
           "ltd":"Limited",
           "inc":"incorporated",
           "e.":"east",
           "sch.":"School",
           "corpo":"Corporation",
            "sch":"School",
           "ent":"Entrance",
           "ent.":"Entrance",
           "n.":"North",
           "brdg.":"Bridge", 
           "brdg":"Bridge", 
           "ave.":"Avenue",
           "sta.":"Station",
            "sta":"Station",
            "mae":"In Front Of",
            "galli":"alley",
            "ave": "Avenue",
           "extn":"Extension",
           "opp.":"Opposite",
           "opp":"Opposite",
           "mt.":"Mountain",
           "mt":"Mountain",
            "rd": "Road",
            "w": "West",
            "n": "North",
            "s": "South",
            "e": "East",
            "blvd":"Boulevard",
            "sr": "Drive",
            "ct": "Court",
            "ne": "Northeast",
            "se": "Southeast",
            "nw": "Northwest",
            "sw": "Southwest",
            "dr": "Drive",
            "sq": "Square",
            "ln": "Lane",
            "trl": "Trail",
            "pkwy": "Parkway",
            "ste": "Suite",
            "lp": "Loop",
            "hwy": "Highway",
            "cir": "Circle"}


def audit_street_type(street_types, street_name):
    m = street_type_re.search(street_name)
    if m:
        street_type = m.group()
        if street_type not in expected:
            street_types[street_type].add(street_name)
            #print (street_types[street_type],street_type)


def is_street_name(elem):
    return (elem.attrib['k'] == "addr:street" or elem.attrib["k"] == "name:en")



def audit(osmfile):
    osm_file = open(osmfile, "r")
    street_types = defaultdict(set)
    for event, elem in ET.iterparse(osm_file, events=("start",)):

        if elem.tag == "node" or elem.tag == "way":
            for tag in elem.iter("tag"):
                if is_street_name(tag):
                    audit_street_type(street_types, tag.attrib['v'])
    return street_types

#  The function takes a string with street name as an argument and should return the fixed name
def update_name(name, mapping):
    edited = []
    for each in name.split(" "):
        each =each.strip(",\.").lower()
        if each in mapping.keys():
            each = mapping[each]
        edited.append(each.capitalize())
    # Return all pieces of the name as a string joined by a space.
    return " ".join(edited)

def test_fix_names(File):
    for f in File:
        city_types = audit(f)

        #hoodi_types = audit(hoodi)

        print "Fixed Names of:",f
        for st_type, ways in city_types.iteritems():
            for name in ways:
                better_name = update_name(name, mapping)
                #uncomment below to print all names
                # if name != better_name and  :
                #     print name, "=>", better_name
                #comment below to print all names if above is uncommented
                if "." in name:
                    print name, "=>", better_name
        print("\n")
def clean_name(name):
        return(update_name(name, mapping))

if __name__ == '__main__':
    test_fix_names(File)
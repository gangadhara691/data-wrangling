
# coding: utf-8

# **P3 - Data Wrangling with MongoDB**
# **OpenStreetMap Project Data Wrangling with MongoDB**
## Gangadhara Naga Sai<a name="top"></a>
# Data used from  https://mapzen.com/metro-extracts/  
#MapZen Weekly OpenStreetMaps Metro Extracts


# Map Areas:
#    These two maps are selected since ,right now i am living at Hoodi,Bengaluru. And my dream is to do my masters in japan in robotics,so i had selected locality of University of tokyo, Bunkyo.I really wanted to explore differences between the regions. 

## Bonkyu,Tokyo,Japan 
# https://mapzen.com/data/metro-extracts/your-extracts/fdd7c4ef0518  
## Hoodi,Bengaluru,india 
# https://mapzen.com/data/metro-extracts/your-extracts/f9bfd35cd2ab

#########################################################################
########### All the files are available at bitbucket ####################
#########################################################################

# https://bitbucket.org/gangadhara691/data-wrangling/src

#Or unzip both the zipped map files available   
    
#importing libraries
import os
import xml.etree.cElementTree as ET
import pprint
import re
import codecs
import json
from collections import defaultdict
import bson
import pymongo
import re

# loading osm files
DATADIR = "."
FILE1 = "tokyo1.osm"
FILE2 = "bangalore.osm"
Bunkyo = os.path.join(DATADIR, FILE1)
hoodi= os.path.join(DATADIR, FILE2)
File=[Bunkyo,hoodi]
city_names=["bunkyo","hoodi"]
files_json=["tokyo1.osm.json","bangalore.osm.json"]
#############################################################################################################################
#counts total number of tags
from count_tags import diff_tag_number,count_tag_number
count_tag_number(File)
print("\n")
diff_tag_number(File)
print("\n")

###################################################################################################################################
print("------------------------------------------------------------------------------------------------------------")
from audit import test_fix_names
test_fix_names(File)

print("\n")

####################################################################################################################################
print("------------------------------------------------------------------------------------------------------------")
#print("Shaping of element from map.osm to map.json")
from process_map import shape_map,insert_db,get_db
# Get 'project' database
mongo_db = get_db('project')
shape_map([Bunkyo,hoodi],["bunkyo","hoodi"])
insert_db(mongo_db,"cities",files_json)
collection="cities"#the above cities is where the collection is stored
db=mongo_db
print("------------------------------------------------------------------------------------------------------------")

####################################################################################################################
print("\n")
from map_analysis import top_contributors,top_Amenities,Total_nodes,city_nodes,total_ways,total_contributors,places_worship
from map_analysis import popular_restaurants,popular_fastfood,ATMS,martial_arts,popular_shops,popular_supermarkets
print ("Top contributor's User  Name:")

result = mongo_db.cities.aggregate([{"$match":{"created.user":{"$exists":1}}},
                {"$group": 
                 {"_id": {"City":"$city",
                          "User":"$created.user"},
                            "contribution": {"$sum": 1}}},                            
                 {"$project": {'_id':0,
                               "City":"$_id.City",
                               "User_Name":"$_id.User",
                               "Total_contribution":"$contribution"}},
                 {"$sort": {"Total_contribution": -1}},
                 {"$limit" : 5 }])
for each in result:    
    print(each)
print("\n")
print("------------------------------------------------------------------------------------------------------------")

####################################################################################################################

# ### To get the top contributors according to each city differently we use the pipeline query structure of mongo db
# #### Where to select different cities we use pipeline fuction given below for the rest of the queries
# #### we just need to update the p variable in pipeline function

from map_analysis import pipeline_output
pipeline_output(collection,city_names,db,top_contributors)
print("The top contributors for hoodi are no where near since bunkyo being a more compact region than hoodi ,there are more places to contribute.")
print("\n")



####################################################################################################################

print("\n")
print("------------------------------------------------------------------------------------------------------------")

pipeline_output(collection,city_names,db,top_Amenities)

print("As compared to hoodi ,bunkyo have few atms,And parking can be commonly found in bunkyo locality")

####################################################################################################################
print("\n")
print("------------------------------------------------------------------------------------------------------------")


# Total nodes in database.
Total_nodes(collection,city_names,db)

# Number of node nodes.
city_nodes(collection,city_names,db)

# Number of way nodes.
total_ways(collection,city_names,db)

# Number of constributors.
total_contributors(collection,city_names,db)
####################################################################################################################
print("------------------------------------------------------------------------------------------------------------")
print("\n")
##  popular places of worship
pipeline_output(collection,city_names,db,places_worship)

print("As expected japan is popular with buddism,\
but india being a secular country it will be having most of the reglious places of worship,where hinduism being majority")


####################################################################################################################
print("------------------------------------------------------------------------------------------------------------")


## popular restaurants
pipeline_output(collection,city_names,db,popular_restaurants)

print("Indian style cusine in Bunkyo seems famous, Which will be better if i go to japan and do my higher studies there.")
    

####################################################################################################################

print("\n")
print("------------------------------------------------------------------------------------------------------------")

## popular fast food joints
pipeline_output(collection,city_names,db,popular_fastfood)

print("Burger seems very popular among japanese in fast foods,i was expecting ramen to be more popular \
, but in hoodi pizza is really common,being a metropolitan city.")


####################################################################################################################

print("\n")
print("------------------------------------------------------------------------------------------------------------")

##  ATM's near locality
pipeline_output(collection,city_names,db,ATMS)

print("There are quite a few ATM in Bunkyo as compared to hoodi")


####################################################################################################################
print("\n")
print("------------------------------------------------------------------------------------------------------------")

##  Martial arts or Dojo Center near locality
pipeline_output(collection,city_names,db,martial_arts)

print("I wanted to learn martial arts , In japan is known for its akido and other ninjistsu martial arts , where i can find some in bunkyo Where as in hoodi,india Kalaripayattu Martial Arts are one of the ancient arts that ever existed.")

####################################################################################################################
print("\n")
print("------------------------------------------------------------------------------------------------------------")


## most popular shops.
pipeline_output(collection,city_names,db,popular_shops)

print("The general stores are quite common in both the places")


####################################################################################################################


## most popular supermarkets
print("\n")
print("------------------------------------------------------------------------------------------------------------")
pipeline_output(collection,city_names,db,popular_supermarkets)

print("These are few common supermarket brands in both the cities \
And Nilgiris is like 500 meters away from my home.")
print("-----------------------------------------------------X-------------------------------------------------------")




####################################################### X #############################################################





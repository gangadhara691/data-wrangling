import os
import xml.etree.cElementTree as ET
import pprint
import re
import codecs
import json
from collections import defaultdict


print("Counting total number of tags")
print("\n")

def count_tags(filename):
    counts = defaultdict(int)
    for line in ET.iterparse(filename, events=("start",)):
        current = line[1].tag
        counts[current] += 1
    return counts
def count_tag_number(File):
    for f in File:    
        tags = count_tags(f)
        print "count of total tags of",f
        pprint.pprint(tags)
        print("\n")

# We have a count of each of
# four tag categories in a dictionary:

#   "lower", for tags that contain only lowercase letters and are valid,
#   "lower_colon", for otherwise valid tags with a colon in their names,
#   "problemchars", for tags with problematic characters, and
#   "other", for other tags that do not fall into the other three categories.

lower = re.compile(r'^([a-z]|_)*$')
lower_colon = re.compile(r'^([a-z]|_)*:([a-z]|_)*$')
problemchars = re.compile(r'[=\+/&<>;\'"\?%#$@\,\. \t\r\n]')



def key_type(element, keys):
    if element.tag == "tag":
        
        if lower.match(element.attrib["k"]) is not None:
            keys["lower"]+=1
            #print element.attrib["k"]
        elif lower_colon.match(element.attrib["k"]) is not None:
            keys["lower_colon"]+=1
            #print element.attrib["k"]
        elif problemchars.match(element.attrib["k"]) is not None:
            keys["problemchars"]+=1
            # print element.attrib["k"]
        else:
            keys["other"]+=1
            # print element.attrib["k"]


    return keys



def process_map(filename):
    keys = {"lower": 0, "lower_colon": 0, "problemchars": 0, "other": 0}
    for _, element in ET.iterparse(filename):
        keys = key_type(element, keys)

    return keys

def diff_tag_number(File):
    for f in File:    
        tags = process_map(f)
        print "count of total different tags of",f
        pprint.pprint(tags)
        print("\n")



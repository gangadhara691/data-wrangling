import xml.etree.cElementTree as ET
import pprint
import re
import codecs
import json
from audit import is_street_name,clean_name
print('Shaping element From map.osm to a dict format')
# In[10]:

lower = re.compile(r'^([a-z]|_)*$')
lower_colon = re.compile(r'^([a-z]|_)*:([a-z]|_)*$')
problemchars = re.compile(r'[=\+/&<>;\'"\?%#$@\,\. \t\r\n]')

CREATED = [ "version", "changeset", "timestamp", "user", "uid"]

## Names in Different Language

# Different regions have different languages ,and we find that some of names were in different language which are filltered to get only english names.

# Which would check weather the characters belong to ascii or not
def isEnglish(s):
    try:
        s.encode('ascii')
    except UnicodeEncodeError:
        return False
    else:
        return True

def is_postal_code(elem):
    return (elem.attrib['k'] == "addr:postcode")
def clean_postal_code(postcode,city):
    post=postcode
    if city == "hoodi" and len(re.sub('[\s,+]' ,'', postcode))==6:
        if  re.match(r'^\d{6}$',postcode) :
            post=postcode
        else:
            post= re.sub('[\s,+]' ,'', postcode)
    elif city =="bunkyo" and len(re.sub('[^A-Za-z0-9]+' ,'', postcode))==7:
        if re.match(r'^([0-9]){3}[-]([0-9]){4}$',postcode):
            post=postcode
        else:
            b=re.sub('[^A-Za-z0-9]+' ,'', postcode)
            post= re.sub(r'(\d\d\d)(\d\d\d\d)', r'\1-\2', b)
        
    return  post
          
# function that will parse the map file, and call the function with the element
#as an argument.And Return a dictionary, containing the shaped data for that element.  
def shape_element(element,city):
    node = {}

    #processing only 2 types of top level tags: "node" and "way"
    if element.tag == "node" or element.tag == "way"  :
        #adding city tag so can diferentiate between both cities
        node["city"]= city
   
        node["type"]= element.tag
        
        # - attributes in the CREATED array are added under a key "created"
        for a in element.attrib.keys():
            if a in CREATED :
                
                if 'created' in node.keys():
                    node['created'][a]=element.attrib[a]
                else:
                    node['created']={a:element.attrib[a]}
    
            # - attributes for latitude and longitude are added to a "pos" array,
            #   for use in geospacial indexing.The values inside "pos" array are floats
            #   and not strings.        
            elif (a == "lat" or a == "lon" ):
                node["pos"]= [float(element.attrib['lat']), float(element.attrib["lon"] ) ] 
            
            else:
                node[a]=element.attrib[a]
                
            #   the second level tag "k" value contains problematic characters, it is ignored
            # - if the second level tag "k" value starts with "addr:", it is added to a dictionary "address"
        for child in element:
            #All node refs are appended in to a list with tag node_refs 
            if child.tag == 'nd':
                node.setdefault('node_refs',[]).append(child.attrib['ref'])
            elif child.tag == 'tag':
                #children are of type tag
                k = child.attrib['k']
                v = child.attrib['v']

                #If the tags are of type street name , then clean it
                if is_street_name(child):
                    v = clean_name(v)
                    
                if is_postal_code(child):
                    v = clean_postal_code(v,city)

                if 'addr' in child.attrib.values()[0].split(":"):                
                    if "address" in node.keys():
                        node["address"][k.split(":")[-1]]= v 
                    else:
                        node['address']={k.split(":")[-1] : v}

                # elif(child.tag=="nd"):
                #     if "node_refs" in node.keys():
                #         node["node_refs"].append(child.attrib["ref"]) 
                #     else:
                #         node['node_refs']=[child.attrib["ref"]]

                # - if the second level tag "k" value does not start with "addr:", but contains ":",
                #   It is added to Additional Information tag.
                else:
                    if isEnglish(child.attrib["v"]):
                        if "Additional Information" in node.keys():
                            node["Additional Information"][k]=v
                        else:
                            node["Additional Information"]={k:v}
       
        return node
    else:
        return None


def process_map(file_in, city, pretty = False):
    
    file_out = "{0}.json".format(file_in)
    data = []
    with codecs.open(file_out, "w") as fo:
        for _, element in ET.iterparse(file_in):
            el = shape_element(element, city)
            if el:
                data.append(el)
                if pretty:
                    fo.write(json.dumps(el, indent=2)+"\n")
                else:
                    fo.write(json.dumps(el) + "\n")
                #break
    return data
#To connect to mongo db through pymongo using mongo client
from pymongo import MongoClient
def get_db(db_name):
    client = MongoClient("mongodb://localhost:27017")
    db = client[db_name]
    return db

#inserting data into database
def insert_data(j,collection,db):
    with open(j, 'r') as f:
        for each in f.readlines():
            db[collection].insert(json.loads(each))
    print "inserted processed data into database: ", j


# # Both cities merging in a single collection
# ## We add "city" tag to categorize and differentiate them from one another
def shape_map(File,city):
    for f,c in zip(File,city):
        data = process_map(f, c, False)
    #return(data)
def insert_db(db,collection,File_json):
    # Get 'project' database
    #mongo_db = get_db('project')
    for fj in File_json:
        city_data = fj  
        insert_data(city_data,collection,db)
        print("\n")

def test_postcode(filename,city):
      for _, element in ET.iterparse(filename):
        
        if element.tag == "tag":
            if is_postal_code(element):
                
                print clean_postal_code(element.attrib["v"],city) ,element.attrib["v"]
       
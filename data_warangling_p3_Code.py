
# coding: utf-8

# **P3 - Data Wrangling with MongoDB**
# **OpenStreetMap Project Data Wrangling with MongoDB**
## Gangadhara Naga Sai<a name="top"></a>
# Data used from  https://mapzen.com/metro-extracts/  
#MapZen Weekly OpenStreetMaps Metro Extracts


# Map Areas:
#    These two maps are selected since ,right now i am living at Hoodi,Bengaluru. And my dream is to do my masters in japan in robotics,so i had selected locality of University of tokyo, Bunkyo.I really wanted to explore differences between the regions. 

## Bonkyu,Tokyo,Japan 
# https://mapzen.com/data/metro-extracts/your-extracts/fdd7c4ef0518  
## Hoodi,Bengaluru,india 
# https://mapzen.com/data/metro-extracts/your-extracts/f9bfd35cd2ab

#########################################################################
############## All the files are available at bitbucket #################
#########################################################################

# https://bitbucket.org/gangadhara691/data-wrangling/src

#Or unzip both the zipped map files available   
    
#importing libraries
import os
import xml.etree.cElementTree as ET
import pprint
import re
import codecs
import json
from collections import defaultdict
import bson
import pymongo


# loading osm files
DATADIR = "."
FILE1 = "tokyo1.osm"
FILE2 = "bangalore.osm"
Bunkyo = os.path.join(DATADIR, FILE1)
hoodi= os.path.join(DATADIR, FILE2)

#############################################################################################################################
#counts total number of tags
print("Counts total number of tags")
print("\n")

def count_tags(filename):
    counts = defaultdict(int)
    for line in ET.iterparse(filename, events=("start",)):
        current = line[1].tag
        counts[current] += 1
    return counts

bunkyotags = count_tags(Bunkyo)
hooditags = count_tags(hoodi)
print("bunkyotags count:")
pprint.pprint(bunkyotags)
print("\n")
print("hooditags count:")
pprint.pprint(hooditags)
print("\n")

##################################################################################################################################
print("------------------------------------------------------------------------------------------------------------")

# We have a count of each of
# four tag categories in a dictionary:

#   "lower", for tags that contain only lowercase letters and are valid,
#   "lower_colon", for otherwise valid tags with a colon in their names,
#   "problemchars", for tags with problematic characters, and
#   "other", for other tags that do not fall into the other three categories.

lower = re.compile(r'^([a-z]|_)*$')
lower_colon = re.compile(r'^([a-z]|_)*:([a-z]|_)*$')
problemchars = re.compile(r'[=\+/&<>;\'"\?%#$@\,\. \t\r\n]')



def key_type(element, keys):
    if element.tag == "tag":
        
        if lower.match(element.attrib["k"]) is not None:
            keys["lower"]+=1
            #print element.attrib["k"]
        elif lower_colon.match(element.attrib["k"]) is not None:
            keys["lower_colon"]+=1
            #print element.attrib["k"]
        elif problemchars.match(element.attrib["k"]) is not None:
            keys["problemchars"]+=1
            # print element.attrib["k"]
        else:
            keys["other"]+=1
            # print element.attrib["k"]


    return keys



def process_map(filename):
    keys = {"lower": 0, "lower_colon": 0, "problemchars": 0, "other": 0}
    for _, element in ET.iterparse(filename):
        keys = key_type(element, keys)

    return keys


print("\n")

bunkyotags  = process_map(Bunkyo)
hooditags= process_map(hoodi)
print("bunkyotags:")
pprint.pprint(bunkyotags)
print("\n")
print("hooditags:")
pprint.pprint(hooditags)
print("\n")

###################################################################################################################################
print("------------------------------------------------------------------------------------------------------------")

print("Auditing Over-Abbreviated Names:")
#Auditing Over-Abbreviated Names:
# Since the most of data being manually uploaded, there are lot of abbreviations in street names,locality names.
# Where they are filtered and replaced with full names.


street_type_re = re.compile(r'\b\S+\.?$', re.IGNORECASE)


expected = ["Street", "Avenue", "Boulevard", "Drive", "Court","Apartment",
            "Place", "Square", "Lane", "Road", "Trail", "Parkway", "Commons",
           "Highway", "Loop", "Circle", "Walk", "Way", "Southwest", "Northeast",
           "Southeast", "Northwest"]

# UPDATE THIS VARIABLE
mapping = { "pl": "Place",
            "st": "Saint",
           "st ":"Saint",
            "ph" :"Phase",
            "apt" :"Apartment",
            "ku" :"City",
            't':"Town",
           "jr":"Junior",
           "d":"Door",
           "bld":"Building",
           "univ.": "University",
            "univ": "University",
           "bldg.":"Building",
           "bldg":"Building",
           "Co.":"Company",
           "co":"Company",
           "Co.,Ltd.":"Company Limited",
           "ltd":"Limited",
           "inc":"incorporated",
           "e.":"east",
           "sch.":"School",
           "corpo":"Corporation",
            "sch":"School",
           "ent":"Entrance",
           "ent.":"Entrance",
           "n.":"North",
           "brdg.":"Bridge", 
           "brdg":"Bridge", 
           "ave.":"Avenue",
           "sta.":"Station",
            "sta":"Station",
            "mae":"In Front Of",
            "galli":"alley",
            "ave": "Avenue",
           "extn":"Extension",
           "opp.":"Opposite",
           "opp":"Opposite",
           "mt.":"Mountain",
           "mt":"Mountain",
            "rd": "Road",
            "w": "West",
            "n": "North",
            "s": "South",
            "e": "East",
            "blvd":"Boulevard",
            "sr": "Drive",
            "ct": "Court",
            "ne": "Northeast",
            "se": "Southeast",
            "nw": "Northwest",
            "sw": "Southwest",
            "dr": "Drive",
            "sq": "Square",
            "ln": "Lane",
            "trl": "Trail",
            "pkwy": "Parkway",
            "ste": "Suite",
            "lp": "Loop",
            "hwy": "Highway",
            "cir": "Circle"}


def audit_street_type(street_types, street_name):
    m = street_type_re.search(street_name)
    if m:
        street_type = m.group()
        if street_type not in expected:
            street_types[street_type].add(street_name)
            #print (street_types[street_type],street_type)


def is_street_name(elem):
    return (elem.attrib['k'] == "addr:street" or elem.attrib["k"] == "name:en")



def audit(osmfile):
    osm_file = open(osmfile, "r")
    street_types = defaultdict(set)
    for event, elem in ET.iterparse(osm_file, events=("start",)):

        if elem.tag == "node" or elem.tag == "way":
            for tag in elem.iter("tag"):
                if is_street_name(tag):
                    audit_street_type(street_types, tag.attrib['v'])
    return street_types

#  The function takes a string with street name as an argument and should return the fixed name
def update_name(name, mapping):
    edited = []
    for each in name.split(" "):
        each =each.strip(",\.").lower()
        if each in mapping.keys():
            each = mapping[each]
        edited.append(each.capitalize())
    # Return all pieces of the name as a string joined by a space.
    return " ".join(edited)


bunkyo_types = audit(Bunkyo)

hoodi_types = audit(hoodi)

print('Bunkyo Fixed Names:')
for st_type, ways in bunkyo_types.iteritems():
    for name in ways:
        better_name = update_name(name, mapping)
        #uncomment below to print all names
        if "." in name:
            print name, "=>", better_name

print("\n")
# print('Hoodi Fixed Names:')
for st_type, ways in hoodi_types.iteritems():
    for name in ways:
        better_name = update_name(name, mapping)
        #uncomment below to print all names
        # if name != better_name and  :
        #     print name, "=>", better_name
print("\n")

####################################################################################################################################
print("------------------------------------------------------------------------------------------------------------")

print('Shaping element From map.osm to a map.osm.json')
# In[10]:

lower = re.compile(r'^([a-z]|_)*$')
lower_colon = re.compile(r'^([a-z]|_)*:([a-z]|_)*$')
problemchars = re.compile(r'[=\+/&<>;\'"\?%#$@\,\. \t\r\n]')

CREATED = [ "version", "changeset", "timestamp", "user", "uid"]

## Names in Different Language

# Different regions have different languages ,and we find that some of names were in different language which are filltered to get only english names.

# Which would check weather the characters belong to ascii or not

def isEnglish(s):
    try:
        s.encode('ascii')
    except UnicodeEncodeError:
        return False
    else:
        return True
    
# function that will parse the map file, and call the function with the element
#as an argument.And Return a dictionary, containing the shaped data for that element.  
def shape_element(element,city):
    node = {}

    #processing only 2 types of top level tags: "node" and "way"
    if element.tag == "node" or element.tag == "way"  :
        #adding city tag so can diferentiate between both cities
        node["city"]= city
   
        node["type"]= element.tag
        
        # - attributes in the CREATED array are added under a key "created"
        for a in element.attrib.keys():
            if a in CREATED :
                
                if 'created' in node.keys():
                    node['created'][a]=element.attrib[a]
                else:
                    node['created']={a:element.attrib[a]}
    
            # - attributes for latitude and longitude are added to a "pos" array,
            #   for use in geospacial indexing.The values inside "pos" array are floats
            #   and not strings.        
            elif (a == "lat" or a == "lon" ):
                node["pos"]= [float(element.attrib['lat']), float(element.attrib["lon"] ) ] 
            
            else:
                node[a]=element.attrib[a]
                
            #   the second level tag "k" value contains problematic characters, it is ignored
            # - if the second level tag "k" value starts with "addr:", it is added to a dictionary "address"
        for child in element:
            if 'addr' in child.attrib.values()[0].split(":"):                
                if "address" in node.keys():
                    node["address"][child.attrib["k"].split(":")[-1]]= child.attrib["v"] 
                else:
                    node['address']={child.attrib["k"].split(":")[-1] : child.attrib["v"]}
            #All node refs are appended in to a list with tag node_refs      
            elif(child.tag=="nd"):
                if "node_refs" in node.keys():
                    node["node_refs"].append(child.attrib["ref"]) 
                else:
                    node['node_refs']=[child.attrib["ref"]]
                    
            # - if the second level tag "k" value does not start with "addr:", but contains ":",
            #   It is added to Additional Information tag.
            else:
                if isEnglish(child.attrib["v"]):
                    if "Additional Information" in node.keys():
                        node["Additional Information"][child.attrib["k"]]=child.attrib["v"]
                    else:
                        node["Additional Information"]={child.attrib["k"]:child.attrib["v"]}
       
        return node
    else:
        return None


def process_map(file_in, city, pretty = False):
    
    file_out = "{0}.json".format(file_in)
    data = []
    with codecs.open(file_out, "w") as fo:
        for _, element in ET.iterparse(file_in):
            el = shape_element(element, city)
            if el:
                data.append(el)
                if pretty:
                    fo.write(json.dumps(el, indent=2)+"\n")
                else:
                    fo.write(json.dumps(el) + "\n")
                #break
    return data
#To connect to mongo db through pymongo using mongo client
from pymongo import MongoClient
def get_db(db_name):
    client = MongoClient("mongodb://localhost:27017")
    db = client[db_name]
    return db

#inserting data into database
def insert_data(j,collection,db):
    with open(j, 'r') as f:
        for each in f.readlines():
            db[collection].insert(json.loads(each))
    print "inserted processed data into database: ", j


# # Both cities merging in a single collection
# ## We add "city" tag to categorize and differentiate them from one another

data = process_map(Bunkyo, 'bunkyo', False)
data = process_map(hoodi, 'hoodi', False)

# Get 'project' database
mongo_db = get_db('project')

tokyo_data = 'tokyo1.osm.json'  
insert_data(tokyo_data, 'cities',mongo_db)
print("\n")
banglore_data = 'bangalore.osm.json' 
insert_data(banglore_data, 'cities',mongo_db)
print("------------------------------------------------------------------------------------------------------------")

############################################################################################################################

print("\n")

print ("Top contributor's User  Name grouped by city:")

result = mongo_db.cities.aggregate([{"$match":{"created.user":{"$exists":1}}},
                {"$group": 
                 {"_id": {"City":"$city",
                          "User":"$created.user"},
                            "contribution": {"$sum": 1}}},                            
                 {"$project": {'_id':0,
                               "City":"$_id.City",
                               "User_Name":"$_id.User",
                               "Total_contribution":"$contribution"}},
                 {"$sort": {"Total_contribution": -1}},
                 {"$limit" : 5 }])
for each in result:    
    print(each)
print("\n")
print("------------------------------------------------------------------------------------------------------------")

####################################################################################################################
print("To get the top contributors:")
# ### To get the top contributors according to each city differently we use the pipeline query structure of mongo db
# #### Where to select different cities we use pipeline fuction given below for the rest of the queries
# #### we just need to update the p variable in pipeline function


def pipeline(city):
    p= [{"$match":{"created.user":{"$exists":1},
                                          "city":city}},
                 {"$group": {"_id": {"City":"$city",
                                     "User":"$created.user"},
                            "contribution": {"$sum": 1}}},                            
                 {"$project": {'_id':0,
                               "City":"$_id.City",
                               "User_Name":"$_id.User",
                               "Total_contribution":"$contribution"}},
                 {"$sort": {"Total_contribution": -1}},
                 {"$limit" : 5 }]
    return p
result1 =mongo_db["cities"].aggregate(pipeline('bunkyo'))
for each in result1:    
    print(each)
print("\n")
result2 =mongo_db["cities"].aggregate(pipeline('hoodi'))
for each in result2:    
    print(each)
print("\n")
print("The top contributors for hoodi are no where near since bunkyo being a more compact region than hoodi ,there are more places to contribute.")
print("\n")



####################################################################################################################

print("\n")
print("------------------------------------------------------------------------------------------------------------")

print("To get the top Amenities in Hoodi and Bunkyo:")
# ### To get the top Amenities in Hoodi and Bunkyo

# In[25]:

def pipeline(city):
    p = [{"$match":{"Additional Information.amenity":{"$exists":1},
                                          "city":city}},
                 {"$group": {"_id": {"City":"$city",
                                    "Amenity":"$Additional Information.amenity"},
                            "count": {"$sum": 1}}},
                 {"$project": {'_id':0,
                               "City":"$_id.City",
                               "Amenity":"$_id.Amenity",
                               "Count":"$count"}},
                 {"$sort": {"Count": -1}},
                 {"$limit" : 10 }]
    return p


result1 =mongo_db["cities"].aggregate(pipeline('bunkyo'))
for each in result1:    
    print(each)
print("\n")
result2 =mongo_db["cities"].aggregate(pipeline('hoodi'))
for each in result2:    
    print(each)
print("\n")
print("As compared to hoodi ,bunkyo have few atms,And parking can be commonly found in bunkyo locality")

####################################################################################################################
print("\n")
print("------------------------------------------------------------------------------------------------------------")

print("2. Data Overview")
print("\n")
print("Total nodes in database")

# Total nodes in database.
print("Total nodes in database")
print "Bunkyo:",mongo_db.cities.find({'city':'bunkyo'}).count()
print "Hoodi:",mongo_db.cities.find({'city':'hoodi'}).count()




# Number of node nodes.
print("\n")
print("Number of node nodes:")
print "Bunkyo:",mongo_db.cities.find({"type":"node",
                                    'city':'bunkyo'}).count()
print "Hoodi:",mongo_db.cities.find({"type":"node",
                                    'city':'hoodi'}).count()




# Number of way nodes.
print("\n")
print("Number of way nodes")
print "Bunkyo:",mongo_db.cities.find({'type':'way',
                                  'city':'bunkyo'}).count()
print "Hoodi:",mongo_db.cities.find({'type':'way',
                                  'city':'hoodi'}).count()


# Number of constributors.
print("\n")
print("Number of contributors")
print "Contributors:", len(mongo_db.cities.distinct("created.user"))
####################################################################################################################
print("------------------------------------------------------------------------------------------------------------")

print("\n")
print("most popular amenities:")

## most popular amenities
def pipeline(city):
    p = [{"$match":{"Additional Information.amenity":{"$exists":1},
                                          "city":city}},
                       {"$group":{"_id":{"City":"$city",
                                  "Amenity":"$Additional Information.amenity"},
                           "count":{"$sum":1}}},
                       {"$project": {'_id':0,
                                     "City":"$_id.City",
                                     "Amenity":"$_id.Amenity",
                                     "Count":"$count"}},
                       {"$sort":{"Count":-1}},
                       {"$limit":10}]
    return p
result1 =mongo_db["cities"].aggregate(pipeline('bunkyo'))
for each in result1:    
    print(each)
print("\n")
result2 =mongo_db["cities"].aggregate(pipeline('hoodi'))
for each in result2:    
    print(each)
print("\n")
print("As compared to hoodi ,bunkyo have few atms,And parking can be commonly found in bunkyo locality")

####################################################################################################################
print("------------------------------------------------------------------------------------------------------------")
print("\n")
print("Popular places of worship:")

##  popular places of worship
def pipeline(city):
    p = [{"$match":{"Additional Information.amenity":{"$exists":1},
                                  "Additional Information.amenity":"place_of_worship",
                                  "city":city}},
                       {"$group":{"_id": {"City":"$city",
                                          "Religion":"$Additional Information.religion"},
                                  "count":{"$sum":1}}},
                       {"$project":{"_id":0,
                                    "City":"$_id.City",
                                    "Religion":"$_id.Religion",
                                    "Count":"$count"}},
                       {"$sort":{"Count":-1}},
                       {"$limit":6}]
    return p
result1 =mongo_db["cities"].aggregate(pipeline('bunkyo'))
for each in result1:    
    print(each)
print("\n")
result2 =mongo_db["cities"].aggregate(pipeline('hoodi'))
for each in result2:    
    print(each)
print("\n")
print("As expected japan is popular with buddism,\
but india being a secular country it will be having most of the reglious places of worship,where hinduism being majority")


####################################################################################################################
print("------------------------------------------------------------------------------------------------------------")


## popular restaurants
print("\n")
print("popular restaurants")

def pipeline(city):
    p = [{"$match":{"Additional Information.amenity":{"$exists":1},
                                 "Additional Information.amenity":"restaurant",
                                 "city":city}},
                      {"$group":{"_id":{"City":"$city",
                                        "Food":"$Additional Information.cuisine"},
                                 "count":{"$sum":1}}},
                      {"$project":{"_id":0,
                                  "City":"$_id.City",
                                  "Food":"$_id.Food",
                                  "Count":"$count"}},
                      {"$sort":{"Count":-1}}, 
                      {"$limit":6}]
    return p

result1 =mongo_db["cities"].aggregate(pipeline('bunkyo'))
for each in result1:    
    print(each)
print("\n")
result2 =mongo_db["cities"].aggregate(pipeline('hoodi'))
for each in result2:    
    print(each)
print("\n")
print("Indian style cusine in Bunkyo seems famous, Which will be better if i go to japan and do my higher studies there.")
    

####################################################################################################################


## popular fast food joints
print("\n")
print("------------------------------------------------------------------------------------------------------------")

print("popular fast food joints")

def pipeline(city):
    p = [{"$match":{"Additional Information.amenity":{"$exists":1},
                                 "Additional Information.amenity":"fast_food",
                                 "city":city}},
                      {"$group":{"_id":{"City":"$city",
                                        "Food":"$Additional Information.cuisine"},
                                 "count":{"$sum":1}}},
                      {"$project":{"_id":0,
                                  "City":"$_id.City",
                                  "Food":"$_id.Food",
                                  "Count":"$count"}},
                      {"$sort":{"Count":-1}}, 
                      {"$limit":6}]
    return p
result1 =mongo_db["cities"].aggregate(pipeline('bunkyo'))
for each in result1:    
    print(each)
print("\n")
result2 =mongo_db["cities"].aggregate(pipeline('hoodi'))
for each in result2:    
    print(each)
print("\n")
print("Burger seems very popular among japanese in fast foods,i was expecting ramen to be more popular \
, but in hoodi pizza is really common,being a metropolitan city.")


####################################################################################################################


##  ATM's near locality
print("\n")
print("------------------------------------------------------------------------------------------------------------")

print("ATM's near locality")

def pipeline(city):
    p = [{"$match":{"Additional Information.amenity":{"$exists":1},
                                 "Additional Information.amenity":"atm",
                                 "city":city}},
                      {"$group":{"_id":{"City":"$city",
                                        "Name":"$Additional Information.name:en"},
                                 "count":{"$sum":1}}},
                      {"$project":{"_id":0,
                                  "City":"$_id.City",
                                  "Name":"$_id.Name",
                                  "Count":"$count"}},
                      {"$sort":{"Count":-1}}, 
                      {"$limit":6}]
    return p

result1 =mongo_db["cities"].aggregate(pipeline('bunkyo'))
for each in result1:    
    print(each)
print("\n")
result2 =mongo_db["cities"].aggregate(pipeline('hoodi'))
for each in result2:    
    print(each)
print("\n")
print("There are quite a few ATM in Bunkyo as compared to hoodi")


####################################################################################################################

##  Martial arts or Dojo Center near locality
print("\n")
print("------------------------------------------------------------------------------------------------------------")

print("Martial arts or Dojo Center near locality")

import re
#Here i use regex to find the word dojo in Additional Information.name and Additional Information.amenity
pat = re.compile(r'dojo', re.I)
d=mongo_db.cities.aggregate([{"$match":{ "$or": [ { "Additional Information.name": {'$regex': pat}}
                                                   ,{"Additional Information.amenity": {'$regex': pat}}]}}
                            ,{"$group":{"_id":{"City":"$city"
                             , "Sport":"$Additional Information.name"}}}])
for each in d:    
    print(each)

print("\n")
print("I wanted to learn martial arts , In japan is known for its akido and other ninjistsu martial arts , where i can find some in bunkyo Where as in hoodi,india Kalaripayattu Martial Arts are one of the ancient arts that ever existed.")

####################################################################################################################


## most popular shops.
print("\n")
print("------------------------------------------------------------------------------------------------------------")

print("most popular shops")

def pipeline(city):
    p = [{"$match":{"Additional Information.shop":{"$exists":1},
                                          "city":city}},
                       {"$group":{"_id":{"City":"$city",
                                  "Shop":"$Additional Information.shop"},
                           "count":{"$sum":1}}},
                       {"$project": {'_id':0,
                                     "City":"$_id.City",
                                     "Shop":"$_id.Shop",
                                     "Count":"$count"}},
                       {"$sort":{"Count":-1}},
                       {"$limit":10}]
    return p

result1 =mongo_db["cities"].aggregate(pipeline('bunkyo'))
for each in result1:    
    print(each)
print("\n")
result2 =mongo_db["cities"].aggregate(pipeline('hoodi'))
for each in result2:    
    print(each)
print("\n")
print("The general stores are quite common in both the places")


####################################################################################################################


## most popular supermarkets
print("\n")
print("------------------------------------------------------------------------------------------------------------")

print("most popular supermarkets")

def pipeline(city):
    p = [{"$match":{"Additional Information.shop":{"$exists":1},
                           "city":city,
                           "Additional Information.shop":"supermarket"}},
                       {"$group":{"_id":{"City":"$city",
                                  "Supermarket":"$Additional Information.name"},
                           "count":{"$sum":1}}},
                       {"$project": {'_id':0,
                                     "City":"$_id.City",
                                     "Supermarket":"$_id.Supermarket",
                                     "Count":"$count"}},
                       {"$sort":{"Count":-1}},
                       {"$limit":5}]
    return p

result1 =mongo_db["cities"].aggregate(pipeline('bunkyo'))
for each in result1:    
    print(each)
print("\n")
result2 =mongo_db["cities"].aggregate(pipeline('hoodi'))
for each in result2:    
    print(each)
print("\n")
print("These are few common supermarket brands in both the cities \
And Nilgiris is like 500 meters away from my home.")
print("-----------------------------------------------------X-------------------------------------------------------")




####################################################### X #############################################################




